import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/storecartswidget/storecartwidget.dart';

class StoreCartScreen extends StatelessWidget {
  const StoreCartScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(
        title: Text(args['storename']),
      ),
      body: StoreCartWidget(vendorid: args['vendorid']),
    );
  }
}
