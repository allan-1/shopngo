import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/storeswidget/storeitem/storeitemcolum.dart';

class StoreItemScreen extends StatelessWidget {
  const StoreItemScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(title: Text(args['vendorname'])),
      body: StoreItemColumn(
          authperson: args['authperson'], vendorid: args['vendorid']),
    );
  }
}
