import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/cartwidget/cartwidget.dart';

class CartItemsScreen extends StatelessWidget {
  const CartItemsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Cart')),
      body: const CartWidget(),
    );
  }
}
