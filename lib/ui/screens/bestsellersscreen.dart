import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/homepage/sections/bestsellersection.dart';

class BestsellerScreen extends StatelessWidget {
  const BestsellerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Best Sellers')),
      body: const BestSellerCategory(),
    );
  }
}
