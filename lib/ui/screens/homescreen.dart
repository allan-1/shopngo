import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';
import 'package:shopngo/ui/screens/cartscreen.dart';
import 'package:shopngo/ui/screens/homepage.dart';
import 'package:shopngo/ui/screens/profilescreen.dart';
import 'package:shopngo/ui/screens/scanscreen.dart';
import 'package:shopngo/ui/screens/storesscreen.dart';
import 'package:shopngo/ui/widgets/drawer/drawerwidget.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Map<String, dynamic>> pages = const [
    {'pages': HomeScreen(), 'title': 'ShopnGo'},
    {'pages': StoreScreen(), 'title': 'ShopnGo'},
    {'pages': ScanScreen(), 'title': 'ShopnGo'},
    {'pages': CartScreen(), 'title': 'ShopnGo'},
    {'pages': ProfileScreen(), 'title': 'ShopnGo'},
  ];

  int selectscreenindex = 0;

  void selectPage(int index) {
    setState(() {
      selectscreenindex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.white,
        elevation: 0,
        title: Text(pages[selectscreenindex]['title']),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pushNamed(context, Routes.favouritesScreen);
              },
              icon: const Icon(
                Icons.favorite,
                color: Colors.red,
              )),
          IconButton(
              onPressed: () {
                Navigator.pushNamed(context, Routes.cartScreen);
              },
              icon: const Icon(Icons.shopping_cart))
        ],
      ),
      body: pages[selectscreenindex]['pages'],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: selectscreenindex,
          selectedItemColor: Colors.green,
          unselectedItemColor: Colors.blueGrey,
          selectedFontSize: 15,
          onTap: selectPage,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(icon: Icon(Icons.store), label: 'Stores'),
            BottomNavigationBarItem(
                icon: Icon(Icons.qr_code_scanner), label: 'Scan'),
            BottomNavigationBarItem(
                icon: Icon(Icons.shopping_cart), label: 'Cart'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Account')
          ]),
      drawer: const DrawerWidget(),
    );
  }
}
