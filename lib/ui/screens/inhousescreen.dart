import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/inhouse%20widget/inhousewidget.dart';

class InhouseScreen extends StatelessWidget {
  const InhouseScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Inhouse Shopping'),
      ),
      body: const InhouseWidget(),
    );
  }
}
