import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/signupwidget/signupwidget.dart';

class SignupScreen extends StatelessWidget {
  const SignupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
      ),
      body: const SignupWidget(),
    );
  }
}
