import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/profileuser/profilewidget.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const ProfileWidget();
  }
}
