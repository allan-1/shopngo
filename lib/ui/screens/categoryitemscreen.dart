import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/categoryitem/categoryitemwidget.dart';

class CategoryItemScreen extends StatelessWidget {
  const CategoryItemScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as Map;

    return Scaffold(
      appBar: AppBar(
        title: Text(args['name']),
      ),
      body: CategoryItemWidget(
        categoryid: args['id'],
      ),
    );
  }
}
