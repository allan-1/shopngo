import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/favouriteswidget/favouriteswidget.dart';

class WishlistScreen extends StatelessWidget {
  const WishlistScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Favourites')),
      body: const FavouritesWidget(),
    );
  }
}
