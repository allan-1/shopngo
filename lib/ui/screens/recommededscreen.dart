import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/recommeded/recommededwidget.dart';

class RecommededScreen extends StatelessWidget {
  const RecommededScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Recommeded Products')),
      body: const RecommededWidget(),
    );
  }
}
