import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/productitem/productitemwidgets.dart';

class ProductItemScreen extends StatelessWidget {
  const ProductItemScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(
        title: Text(args['name']),
        actions: [
          IconButton(onPressed: () {}, icon: const Icon(Icons.shopping_cart))
        ],
      ),
      body: ProductItemWIdget(itemcode: args['id']),
    );
  }
}
