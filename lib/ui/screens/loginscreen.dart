import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/loginwidget/loginwidget.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 0,
      ),
      body: const LoginWidget(),
    );
  }
}
