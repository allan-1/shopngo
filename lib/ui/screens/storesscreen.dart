import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/storeswidget/storeswidget.dart';

class StoreScreen extends StatelessWidget {
  const StoreScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const StoresWidget();
  }
}
