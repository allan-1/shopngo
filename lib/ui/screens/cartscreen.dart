import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/cartwidget/cartwidget.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const CartWidget();
  }
}
