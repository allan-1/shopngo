import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/homepage/homewidget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const HomeWidget();
  }
}
