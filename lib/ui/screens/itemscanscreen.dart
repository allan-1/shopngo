import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/barcodescanner/barcodewidget.dart';

class ItemScan extends StatelessWidget {
  const ItemScan({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Scan BarCode'),
      ),
      body: const BarCodeWidget(),
    );
  }
}
