import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/homepage/sections/categorysection.dart';

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Categories')),
      body: const CategoriesSection(),
    );
  }
}
