import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/barcodescanner/barcodewidget.dart';

class ScanScreen extends StatelessWidget {
  const ScanScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const BarCodeWidget();
  }
}
