import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/storecartitemmodel.dart';
import 'package:shopngo/ui/widgets/storecartswidget/storetile.dart';

class StoreCartWidget extends StatefulWidget {
  final String vendorid;
  const StoreCartWidget({super.key, required this.vendorid});

  @override
  State<StoreCartWidget> createState() => _StoreCartWidgetState();
}

class _StoreCartWidgetState extends State<StoreCartWidget> {
  var storecarts = <StoreCartItemModel>[];
  getStoreCarts() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    ApiServices.getStoreCartsItems(
            sharedPreferences.getInt('id').toString(), widget.vendorid)
        .then((response) {
      if (mounted) {
        setState(() {
          final res = jsonDecode(response.body);
          Iterable list = res['data'];
          storecarts = list
              .map(((model) => StoreCartItemModel.fromJson(model)))
              .toList();
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getStoreCarts();
  }

  @override
  Widget build(BuildContext context) {
    return storecarts.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : ListView.builder(
            itemBuilder: ((context, index) {
              return Storetile(
                  quantity: storecarts[index].quantity,
                  itemname: storecarts[index].itemname,
                  price: storecarts[index].offerprice.toString(),
                  itemimage: storecarts[index].itemimage);
            }),
            itemCount: storecarts.length,
          );
  }
}
