import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/productitem/counter.dart';

class Storetile extends StatefulWidget {
  final String itemname;
  final String price;
  final String itemimage;
  int quantity = 1;
  Storetile({
    super.key,
    required this.itemname,
    required this.price,
    required this.itemimage,
    required this.quantity,
  });

  @override
  State<Storetile> createState() => _StoretileState();
}

class _StoretileState extends State<Storetile> {
  void increment() {
    setState(() {
      widget.quantity += 1;
    });
  }

  void decrement() {
    if (widget.quantity > 1) {
      setState(() {
        widget.quantity -= 1;
      });
    } else {
      setState(() {
        widget.quantity = 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      child: Card(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Image.network(
                    'http://apiforshopandgo.pythonanywhere.com${widget.itemimage}'),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.itemname,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(
                      'Ksh ${widget.price}',
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    IconButton(
                        onPressed: () {},
                        icon: const Icon(
                          Icons.delete,
                          color: Colors.red,
                        )),
                    CounterWidget(
                        counter: widget.quantity,
                        decrement: decrement,
                        increment: increment),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
