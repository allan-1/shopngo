import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/storecartitemmodel.dart';
import 'package:shopngo/models/storecartmodel.dart';
import 'package:shopngo/ui/widgets/cartwidget/cartitemwidget.dart';

class CartWidget extends StatefulWidget {
  const CartWidget({super.key});

  @override
  State<CartWidget> createState() => _CartWidgetState();
}

class _CartWidgetState extends State<CartWidget> {
  var cartstores = <StoreCartModel>[];
  var cartItems = <StoreCartItemModel>[];
  getStoreCarts() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    ApiServices.getStoreCarts(sharedPreferences.getInt('id').toString())
        .then((response) {
      if (mounted) {
        setState(() {
          final res = jsonDecode(response.body);
          Iterable list = res['data'];
          cartstores =
              list.map((model) => StoreCartModel.fromJson(model)).toList();
        });
      }
      getCartStores();
    });
  }

  getCartStores() {
    print('hello world');
    cartstores.map((cart) async {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      ApiServices.getStoreCartsItems(
              sharedPreferences.getInt('id').toString(), cart.vendorid)
          .then((response) {
        final res = jsonDecode(response.body);
        Iterable list = res['data'];
        cartItems =
            list.map(((model) => StoreCartItemModel.fromJson(model))).toList();
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getStoreCarts();
    getCartStores();
  }

  @override
  Widget build(BuildContext context) {
    return cartstores.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : ListView.builder(
            itemBuilder: ((context, index) {
              return CartItemWidget(
                  vendorid: cartstores[index].vendorid,
                  storename: cartstores[index].vendorname,
                  storeimage: cartstores[index].vendorimage,
                  itemcount: cartstores[index].itemcount);
            }),
            itemCount: cartstores.length,
          );
  }
}
