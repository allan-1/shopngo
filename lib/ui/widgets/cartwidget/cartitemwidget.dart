import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class CartItemWidget extends StatelessWidget {
  final String storename;
  final String storeimage;
  final int itemcount;
  final String vendorid;
  const CartItemWidget(
      {super.key,
      required this.storename,
      required this.vendorid,
      required this.storeimage,
      required this.itemcount});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(storename),
        leading: Image.network(
            'http://apiforshopandgo.pythonanywhere.com$storeimage'),
        subtitle: Row(
          children: [
            const Icon(Icons.shopping_bag),
            Text('$itemcount Item(s)')
          ],
        ),
        trailing: ElevatedButton(
          child: const Text('View Items'),
          onPressed: () {
            Navigator.pushNamed(context, Routes.storecartScreen,
                arguments: {'storename': storename, 'vendorid': vendorid});
          },
        ),
      ),
    );
  }
}
