import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/inhouse%20widget/mapwidget.dart';

class InhouseWidget extends StatefulWidget {
  const InhouseWidget({super.key});

  @override
  State<InhouseWidget> createState() => _InhouseWidgetState();
}

class _InhouseWidgetState extends State<InhouseWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height * 0.5,
          child: const MapWidget(),
        ),
        const Text('Shops')
      ],
    );
  }
}
