import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/categories.dart';
import 'package:shopngo/ui/widgets/homepage/sections/productitems.dart';

class CategoryItemWidget extends StatefulWidget {
  final int categoryid;
  const CategoryItemWidget({super.key, required this.categoryid});

  @override
  State<CategoryItemWidget> createState() => _CategoryItemWidgetState();
}

class _CategoryItemWidgetState extends State<CategoryItemWidget> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final categoryitems =
          Provider.of<CategoryProvider>(context, listen: false);
      categoryitems.getCategoryItem(widget.categoryid.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoryitem = Provider.of<CategoryProvider>(context);
    var categitems = categoryitem.categoryitem;
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GridView.builder(
          itemCount: categitems.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3),
          itemBuilder: ((context, index) {
            return ProductItemholder(
                id: categitems[index].itemcode,
                image:
                    'http://apiforshopandgo.pythonanywhere.com/${categitems[index].images}',
                title: categitems[index].itemname,
                price: categitems[index].price.toString());
          })),
    );
  }
}
