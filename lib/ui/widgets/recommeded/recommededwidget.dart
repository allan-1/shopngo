import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/products_provider.dart';
import 'package:shopngo/ui/widgets/homepage/helperwidgets/itemsgrid.dart';

class RecommededWidget extends StatefulWidget {
  const RecommededWidget({super.key});

  @override
  State<RecommededWidget> createState() => _RecommededWidgetState();
}

class _RecommededWidgetState extends State<RecommededWidget> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final bestSellers = Provider.of<ProductsProvider>(context, listen: false);
      bestSellers.getRecommendedProducts();
    });
  }

  @override
  Widget build(BuildContext context) {
    final recommendedModels = Provider.of<ProductsProvider>(context);
    var recommendedlist = recommendedModels.recommededlist;
    return SizedBox(
        height: 350,
        child: recommendedModels.loading
            ? const CircularProgressIndicator()
            : ItemsGrid(itemlist: recommendedlist));
  }
}
