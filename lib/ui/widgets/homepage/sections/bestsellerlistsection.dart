import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/products_provider.dart';
import 'package:shopngo/ui/widgets/homepage/sections/itemswidgets.dart';

class BestSellerListWidget extends StatefulWidget {
  const BestSellerListWidget({super.key});

  @override
  State<BestSellerListWidget> createState() => _BestSellerListWidgetState();
}

class _BestSellerListWidgetState extends State<BestSellerListWidget> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final bestSellers = Provider.of<ProductsProvider>(context, listen: false);
      bestSellers.getBestSellers();
    });
  }

  @override
  Widget build(BuildContext context) {
    final bestsellerModels = Provider.of<ProductsProvider>(context);
    var bestsellerlist = bestsellerModels.bestsellerslised;
    return bestsellerlist.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 160,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: bestsellerlist.length,
                itemBuilder: ((context, index) {
                  return ItemsWidget(
                      image: bestsellerlist[index].image,
                      title: bestsellerlist[index].itemname,
                      price: bestsellerlist[index].price,
                      id: bestsellerlist[index].itemcode);
                })),
          );
  }
}
