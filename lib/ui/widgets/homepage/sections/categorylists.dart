import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/categories.dart';
import 'package:shopngo/ui/widgets/homepage/sections/categorylistholders.dart';

class CategoryListsSection extends StatefulWidget {
  const CategoryListsSection({super.key});

  @override
  State<CategoryListsSection> createState() => _CategoryListsSectionState();
}

class _CategoryListsSectionState extends State<CategoryListsSection> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final categoriesModels =
          Provider.of<CategoryProvider>(context, listen: false);
      categoriesModels.getCategories();
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoriesModels = Provider.of<CategoryProvider>(context);
    var categorylist = categoriesModels.slicedlist;

    return categorylist.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 130,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: categorylist.length,
                itemBuilder: ((context, index) {
                  return CategoryListHolderWidget(
                      id: categorylist[index].id,
                      title: categorylist[index].name,
                      image: categorylist[index].image);
                })),
          );
  }
}
