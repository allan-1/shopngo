import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class ProductItemholder extends StatelessWidget {
  final String image;
  final String id;
  final String title;
  final String price;

  const ProductItemholder(
      {super.key,
      required this.image,
      required this.id,
      required this.title,
      required this.price});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.productItemScreen,
            arguments: {'id': id, 'name': title});
      },
      child: Card(
        elevation: 2,
        child: GridTile(
          header: GridTileBar(
            leading: IconButton(
              icon: const Icon(
                Icons.favorite_border_outlined,
                color: Colors.red,
              ),
              onPressed: () {},
            ),
          ),
          footer: GridTileBar(
            title: Text(title),
            backgroundColor: Colors.black54,
            subtitle: Text(price),
            trailing: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.add_shopping_cart),
            ),
          ),
          child: Image.network(image),
        ),
      ),
    );
  }
}
