import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class CategoryListHolderWidget extends StatelessWidget {
  final String title;
  final String image;
  final int id;
  const CategoryListHolderWidget(
      {super.key, required this.title, required this.image, required this.id});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.categoryItemScreen,
            arguments: {'id': id, 'name': title});
      },
      child: Container(
        margin: const EdgeInsets.all(10),
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            SizedBox(
              height: 70,
              child: Image.network(
                  'http://apiforshopandgo.pythonanywhere.com/$image'),
            ),
            Text(title)
          ],
        ),
      ),
    );
  }
}
