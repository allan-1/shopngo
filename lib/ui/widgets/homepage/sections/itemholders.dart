import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class ItemHolders extends StatelessWidget {
  final String image;
  final String title;
  final int id;
  const ItemHolders(
      {super.key, required this.image, required this.title, required this.id});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.categoryItemScreen,
            arguments: {'id': id, 'name': title});
      },
      child: Container(
        padding: const EdgeInsets.all(10.0),
        // width: 140,
        child: Card(
            elevation: 0,
            child: GridTile(
              footer: Container(
                height: 35,
                color: Colors.green.shade100,
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: Colors.black),
                ),
              ),
              child: Image.network(image),
            )),
      ),
    );
  }
}
