import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/products_provider.dart';
import 'package:shopngo/ui/widgets/homepage/sections/itemswidgets.dart';

class RecommendedProduct extends StatefulWidget {
  const RecommendedProduct({super.key});

  @override
  State<RecommendedProduct> createState() => _TecommendedProductState();
}

class _TecommendedProductState extends State<RecommendedProduct> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final bestSellers = Provider.of<ProductsProvider>(context, listen: false);
      bestSellers.getRecommendedProducts();
    });
  }

  @override
  Widget build(BuildContext context) {
    final recommededModels = Provider.of<ProductsProvider>(context);
    var recommededslised = recommededModels.bestsellerslised;
    return recommededslised.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 160,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: recommededslised.length,
                itemBuilder: ((context, index) {
                  return ItemsWidget(
                      image: recommededslised[index].image,
                      title: recommededslised[index].itemname,
                      price: recommededslised[index].price,
                      id: recommededslised[index].itemcode);
                })),
          );
  }
}
