import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/categories.dart';
import 'package:shopngo/ui/widgets/homepage/sections/itemholders.dart';

class CategoriesSection extends StatefulWidget {
  const CategoriesSection({super.key});

  @override
  State<CategoriesSection> createState() => _CategoriesSectionState();
}

class _CategoriesSectionState extends State<CategoriesSection> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final categoriesModels =
          Provider.of<CategoryProvider>(context, listen: false);
      categoriesModels.getCategories();
    });
  }

  @override
  Widget build(BuildContext context) {
    final categoriesModels = Provider.of<CategoryProvider>(context);
    var categorylist = categoriesModels.categories;

    return categoriesModels.loading
        ? const CircularProgressIndicator()
        : GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3, crossAxisSpacing: 0.5, mainAxisSpacing: 0.5),
            itemBuilder: ((context, index) {
              return ItemHolders(
                image:
                    'http://apiforshopandgo.pythonanywhere.com/${categorylist[index].image}',
                title: categorylist[index].name,
                id: categorylist[index].id,
              );
            }),
            itemCount: categorylist.length,
          );
  }
}
