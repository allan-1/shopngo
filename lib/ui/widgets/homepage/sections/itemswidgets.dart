import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class ItemsWidget extends StatelessWidget {
  final String image;
  final String title;
  final double price;
  final String id;
  const ItemsWidget(
      {super.key,
      required this.image,
      required this.title,
      required this.price,
      required this.id});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.productItemScreen,
            arguments: {'id': id, 'name': title});
      },
      child: Container(
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 90,
              child: Image.network(
                  'http://apiforshopandgo.pythonanywhere.com/$image'),
            ),
            Text(
              title,
              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            Text('Ksh $price')
          ],
        ),
      ),
    );
  }
}
