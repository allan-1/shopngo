import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopngo/providers/products_provider.dart';
import 'package:shopngo/ui/widgets/homepage/helperwidgets/itemsgrid.dart';

class BestSellerCategory extends StatefulWidget {
  const BestSellerCategory({super.key});

  @override
  State<BestSellerCategory> createState() => _BestSellerCategoryState();
}

class _BestSellerCategoryState extends State<BestSellerCategory> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final bestSellers = Provider.of<ProductsProvider>(context, listen: false);
      bestSellers.getBestSellers();
    });
  }

  @override
  Widget build(BuildContext context) {
    final bestsellerModels = Provider.of<ProductsProvider>(context);
    var bestsellerlist = bestsellerModels.bestsellerlist;
    return bestsellerModels.loading
        ? const CircularProgressIndicator()
        : ItemsGrid(itemlist: bestsellerlist);
  }
}
