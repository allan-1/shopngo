import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/homepage/sections/itemswidgets.dart';

class ItemsGrid extends StatelessWidget {
  final List itemlist;
  const ItemsGrid({super.key, required this.itemlist});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 160,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: itemlist.length,
          itemBuilder: ((context, index) {
            return ItemsWidget(
                image: itemlist[index].image,
                title: itemlist[index].itemname,
                price: itemlist[index].price,
                id: itemlist[index].itemcode);
          })),
    );
  }
}
