import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';
import 'package:shopngo/ui/widgets/homepage/helperwidgets/imagebanner.dart';
import 'package:shopngo/ui/widgets/homepage/sections/bestsellerlistsection.dart';
import 'package:shopngo/ui/widgets/homepage/sections/categorylists.dart';
import 'package:shopngo/ui/widgets/homepage/sections/recommendedsection.dart';

class HomeWidget extends StatelessWidget {
  const HomeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.qr_code_scanner),
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.barcodeScreen);
                    },
                  ),
                  hintText: 'What are you searching for ?'),
            ),
            const SizedBox(
              height: 20,
            ),
            const ImageBanner(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Categories',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.categoriesScreen);
                    },
                    child: const Text('See all'))
              ],
            ),
            const CategoryListsSection(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Best Sellers',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.bestsellerScreen);
                    },
                    child: const Text('See more'))
              ],
            ),
            const BestSellerListWidget(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Recommended Products',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                TextButton(onPressed: () {}, child: const Text('See more'))
              ],
            ),
            const RecommendedProduct()
          ],
        ),
      ),
    );
  }
}
