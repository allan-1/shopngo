import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class BarCodeWidget extends StatefulWidget {
  const BarCodeWidget({super.key});

  @override
  State<BarCodeWidget> createState() => _BarCodeWidgetState();
}

class _BarCodeWidgetState extends State<BarCodeWidget> {
  String data = 'No item scanned';

  scan() async {
    await FlutterBarcodeScanner.scanBarcode(
            '#FF0000', 'Cancel', true, ScanMode.BARCODE)
        .then((value) => setState(
              () {
                data = value;
              },
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
              onPressed: (() => scan()), child: const Text('Scan Barcode')),
          Text(data)
        ],
      ),
    );
  }
}
