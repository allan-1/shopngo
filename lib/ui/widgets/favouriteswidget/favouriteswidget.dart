import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/favourites.dart';
import 'package:shopngo/routes/routes.dart';

class FavouritesWidget extends StatefulWidget {
  const FavouritesWidget({super.key});

  @override
  State<FavouritesWidget> createState() => _FavouritesWidgetState();
}

class _FavouritesWidgetState extends State<FavouritesWidget> {
  var favouriteitems = <FavouritesModel>[];
  getFavourites() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    ApiServices.getFavourite(sharedPreferences.getInt('id').toString())
        .then((response) {
      if (mounted) {
        setState(() {
          final res = jsonDecode(response.body);
          Iterable list = res['data'];
          favouriteitems =
              list.map((model) => FavouritesModel.fromJson(model)).toList();
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getFavourites();
  }

  @override
  Widget build(BuildContext context) {
    return favouriteitems.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : ListView.builder(
            itemBuilder: ((context, index) {
              final item = favouriteitems[index];
              return GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, Routes.productItemScreen,
                      arguments: {
                        'id': item.item_code,
                        'name': item.item_name
                      });
                },
                child: Dismissible(
                    key: Key(item.item_code),
                    onDismissed: (direction) {
                      setState(() {
                        favouriteitems.removeAt(index);
                      });
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text('${item.item_name} dismissed')));
                    },
                    background: Container(color: Colors.red),
                    child: ListTile(
                      leading: Image.network(
                          'http://apiforshopandgo.pythonanywhere.com${item.item_images}'),
                      title: Text(item.item_name),
                      subtitle: Text(item.vendor_name),
                      trailing: Text('Ksh ${item.offer_price}'),
                    )),
              );
            }),
            itemCount: favouriteitems.length,
          );
  }
}
