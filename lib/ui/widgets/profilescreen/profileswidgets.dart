import 'package:flutter/material.dart';

class ProfilesWidet extends StatefulWidget {
  const ProfilesWidet({super.key});

  @override
  State<ProfilesWidet> createState() => _ProfilesWidetState();
}

class _ProfilesWidetState extends State<ProfilesWidet> {
  final formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
          key: formkey,
          child: Column(
            children: [
              TextFormField(
                decoration: const InputDecoration(
                    label: Text('Name'), border: OutlineInputBorder()),
              ),
              TextFormField(
                decoration: const InputDecoration(
                    label: Text('Email'), border: OutlineInputBorder()),
              ),
              TextFormField(
                decoration: const InputDecoration(
                    label: Text('Mobile Number'), border: OutlineInputBorder()),
              )
            ],
          )),
    );
  }
}
