import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/storeitemmodel.dart';

class StoreDetails extends StatefulWidget {
  final String vendorid;
  const StoreDetails({super.key, required this.vendorid});

  @override
  State<StoreDetails> createState() => _StoreDetailsState();
}

class _StoreDetailsState extends State<StoreDetails> {
  bool loading = true;
  late StoreItemModel storesdetails;
  getStoresDetails() {
    ApiServices.getVendor(widget.vendorid).then((response) {
      setState(() {
        loading = true;
        final res = jsonDecode(response.body);
        storesdetails = StoreItemModel.fromJson(res['data']);
        loading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getStoresDetails();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SizedBox(
            height: MediaQuery.of(context).size.height * 0.7,
            child: Column(
              children: [
                Image.network(
                    'http://apiforshopandgo.pythonanywhere.com${storesdetails.vendorimage}'),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 3.0),
                  child: Row(
                    children: [
                      const Icon(Icons.social_distance, color: Colors.green),
                      const SizedBox(
                        width: 15,
                      ),
                      Text(storesdetails.distance)
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 3.0),
                  child: Row(
                    children: [
                      const Icon(Icons.directions_bike, color: Colors.green),
                      const SizedBox(
                        width: 15,
                      ),
                      Text(storesdetails.homedelivery),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 3.0),
                  child: Row(
                    children: [
                      const Icon(Icons.shopping_bag, color: Colors.green),
                      const SizedBox(
                        width: 15,
                      ),
                      Text('${storesdetails.noofproduct} Item(s)'),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 3.0),
                  child: Row(
                    children: [
                      const Icon(Icons.mail, color: Colors.green),
                      const SizedBox(
                        width: 15,
                      ),
                      Text(storesdetails.emailid),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 3.0),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.contact_phone,
                        color: Colors.green,
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Text(storesdetails.contactno),
                    ],
                  ),
                ),
              ],
            ),
          );
  }
}
