import 'package:flutter/material.dart';
import 'package:shopngo/ui/widgets/storeswidget/storeitem/storedetails.dart';
import 'package:shopngo/ui/widgets/storeswidget/storeitem/storelist.dart';

class StoreItemColumn extends StatelessWidget {
  final String vendorid;
  final String authperson;
  const StoreItemColumn(
      {super.key, required this.vendorid, required this.authperson});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          StoreDetails(vendorid: vendorid),
          StoreList(authperson: authperson)
        ],
      ),
    );
  }
}
