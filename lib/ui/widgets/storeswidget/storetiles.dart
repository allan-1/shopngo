import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class StoreTile extends StatelessWidget {
  final String storename;
  final String storeimage;
  final String distance;
  final String minoder;
  final String homedelivery;
  final String timeopen;
  final String timeclose;
  final int noproducts;
  final String authperson;
  final String vendorid;
  final String vendorname;
  const StoreTile(
      {super.key,
      required this.storename,
      required this.storeimage,
      required this.distance,
      required this.minoder,
      required this.homedelivery,
      required this.timeopen,
      required this.timeclose,
      required this.noproducts,
      required this.authperson,
      required this.vendorid,
      required this.vendorname});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, Routes.storeitemScreen, arguments: {
          'vendorname': vendorname,
          'vendorid': vendorid,
          'authperson': authperson
        });
      },
      child: Card(
        child: ListTile(
          title: Text(
            storename,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Column(
            children: [
              Row(
                children: [
                  const Icon(
                    Icons.social_distance,
                    color: Colors.green,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(distance)
                ],
              ),
              Row(
                children: [
                  const Icon(
                    Icons.shopping_bag,
                    color: Colors.green,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text('Min order: $minoder')
                ],
              ),
              Row(
                children: [
                  const Icon(
                    Icons.pedal_bike,
                    color: Colors.green,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text(homedelivery)
                ],
              ),
              Row(
                children: [
                  const Icon(
                    Icons.shopping_basket,
                    color: Colors.green,
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Text('$noproducts item(s)')
                ],
              ),
            ],
          ),
          leading: Image.network(
            'http://apiforshopandgo.pythonanywhere.com/$storeimage',
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
