import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/storesmodel.dart';
import 'package:shopngo/ui/widgets/storeswidget/storetiles.dart';

class StoresWidget extends StatefulWidget {
  const StoresWidget({super.key});

  @override
  State<StoresWidget> createState() => _StoresWidgetState();
}

class _StoresWidgetState extends State<StoresWidget> {
  var storeslist = <StoresModel>[];
  getStores() {
    ApiServices.getStoreList().then((response) {
      setState(() {
        final res = jsonDecode(response.body);
        Iterable list = res['data'];
        storeslist = list.map((model) => StoresModel.fromJson(model)).toList();
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getStores();
  }

  @override
  Widget build(BuildContext context) {
    return storeslist.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(
                      hintText: 'Search',
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20)),
                      prefixIcon: const Icon(
                        Icons.search,
                        color: Colors.green,
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.cancel_sharp),
                      )),
                ),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: ListView.builder(
                    itemBuilder: ((context, index) {
                      return StoreTile(
                        storename: storeslist[index].vendorname,
                        storeimage: storeslist[index].vendorimage,
                        distance: storeslist[index].distance,
                        minoder: '${storeslist[index].minimumorder}',
                        homedelivery: storeslist[index].homedelivery,
                        timeopen: storeslist[index].opentime,
                        timeclose: storeslist[index].closetime,
                        noproducts: storeslist[index].noofproducts,
                        authperson: storeslist[index].authperson,
                        vendorid: storeslist[index].vendorid,
                        vendorname: storeslist[index].vendorname,
                      );
                    }),
                    itemCount: storeslist.length,
                  ),
                ),
              ],
            ),
          );
  }
}
