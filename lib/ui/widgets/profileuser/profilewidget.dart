import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class ProfileWidget extends StatelessWidget {
  const ProfileWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          // DrawerHeader(child: Image.asset('assets/images/shopngo.jpeg',)),
          ListTile(
            leading: const Icon(Icons.person),
            title: const Text('Profile'),
            onTap: (() {
              Navigator.pushNamed(context, Routes.profileScreen);
            }),
          ),
          ListTile(
            leading: const Icon(Icons.location_on),
            title: const Text('Address'),
            onTap: () {
              Navigator.pushNamed(context, Routes.profileaddresScreen);
            },
          ),
          ListTile(
            leading: const Icon(Icons.shopping_cart),
            title: const Text('Orders'),
            onTap: () {
              Navigator.pushNamed(context, Routes.categoriesScreen);
            },
          ),
          ListTile(
            leading: const Icon(Icons.payment),
            title: const Text('Payments'),
            onTap: () {
              Navigator.pushNamed(context, Routes.categoriesScreen);
            },
          ),
          ListTile(
            leading: const Icon(Icons.local_offer),
            title: const Text('Rewards'),
            onTap: () {
              Navigator.pushNamed(context, Routes.categoriesScreen);
            },
          ),
          ListTile(
            leading: const Icon(Icons.policy_outlined),
            title: const Text('Cancellations and Refund Policy'),
            onTap: (() {}),
          ),
          ListTile(
            leading: const Icon(Icons.help),
            title: const Text('Help'),
            onTap: (() {}),
          ),
          ListTile(
            leading: const Icon(Icons.info),
            title: const Text('About Us'),
            onTap: (() {}),
          ),
          ListTile(
            leading: const Icon(Icons.policy),
            title: const Text('Privacy Policy'),
            onTap: (() {}),
          ),
          ListTile(
            leading: const Icon(Icons.shield_moon),
            title: const Text('Terms and Conditions'),
            onTap: (() {}),
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: (() {}),
          ),
          ListTile(
            leading: const Icon(Icons.logout),
            title: const Text('Log Out'),
            onTap: (() {}),
          ),
        ],
      ),
    );
  }
}
