import 'package:flutter/material.dart';

class CounterWidget extends StatefulWidget {
  int counter = 1;
  final Function increment;
  final Function decrement;
  CounterWidget(
      {super.key,
      required this.increment,
      required this.decrement,
      required this.counter});

  @override
  State<CounterWidget> createState() => _CounterWidgetState();
}

class _CounterWidgetState extends State<CounterWidget> {
  int counter = 1;
  void increment() {
    setState(() {
      counter += 1;
    });
  }

  void decrement() {
    if (counter > 1) {
      setState(() {
        counter -= 1;
      });
    } else {
      setState(() {
        counter = 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        TextButton(
          onPressed: () {
            widget.decrement();
          },
          style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: const BorderSide(color: Colors.green)))),
          child: const Icon(Icons.remove),
        ),
        Container(
          padding: const EdgeInsets.all(18),
          child: Text('${widget.counter}'),
        ),
        ElevatedButton(
            onPressed: () {
              widget.increment();
            },
            style: ButtonStyle(
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: const BorderSide(color: Colors.green))),
            ),
            child: const Icon(Icons.add)),
      ],
    );
  }
}
