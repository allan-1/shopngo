import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/providers/products_provider.dart';
import 'package:shopngo/ui/widgets/productitem/counter.dart';

class ProductItemWIdget extends StatefulWidget {
  final String itemcode;
  const ProductItemWIdget({super.key, required this.itemcode});

  @override
  State<ProductItemWIdget> createState() => _ProductItemWIdgetState();
}

class _ProductItemWIdgetState extends State<ProductItemWIdget> {
  bool favourite = false;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final productitem = Provider.of<ProductsProvider>(context, listen: false);
      productitem.getProductItem(widget.itemcode);
    });
  }

  int counter = 1;
  void increment() {
    setState(() {
      counter += 1;
    });
  }

  void decrement() {
    if (counter > 1) {
      setState(() {
        counter -= 1;
      });
    } else {
      setState(() {
        counter = 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final productitem = Provider.of<ProductsProvider>(context);
    var proditem = productitem.productitemlist;

    String getTotalprice() {
      double totalprice = proditem[0].offerprice;
      setState(() {
        totalprice = totalprice * counter;
      });
      return totalprice.toString();
    }

    return proditem.isEmpty
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.height * 0.52,
                  child: Image.network(
                    'http://apiforshopandgo.pythonanywhere.com${proditem[0].image}',
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        proditem[0].itemname,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      IconButton(
                          onPressed: () async {
                            SharedPreferences sharedPreferences =
                                await SharedPreferences.getInstance();
                            ApiServices.setFavourite(
                                    sharedPreferences.getInt('id').toString(),
                                    proditem[0].itemcode,
                                    proditem[0].vendorid)
                                .then((response) {
                              setState(() {
                                favourite = !favourite;
                              });
                            });
                          },
                          icon: favourite
                              ? const Icon(
                                  Icons.favorite,
                                  color: Colors.red,
                                )
                              : const Icon(
                                  Icons.favorite_outline,
                                  color: Colors.red,
                                ))
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text(proditem[0].vendorname),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Ksh ${proditem[0].price}',
                          style: const TextStyle(
                              color: Colors.grey,
                              decoration: TextDecoration.lineThrough,
                              fontSize: 20),
                        ),
                        const TextSpan(text: '    '),
                        TextSpan(
                            text: 'Ksh ${proditem[0].offerprice}',
                            style: const TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 20)),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text(proditem[0].description),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: CounterWidget(
                    counter: counter,
                    increment: () {
                      increment();
                    },
                    decrement: () => decrement(),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Ksh ${getTotalprice()}',
                          style: const TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Colors.green),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.56,
                          child: ElevatedButton.icon(
                              onPressed: () async {
                                SharedPreferences sharedPreferences =
                                    await SharedPreferences.getInstance();
                                ApiServices.addToCart(
                                        sharedPreferences
                                            .getInt('id')
                                            .toString(),
                                        proditem[0].itemcode,
                                        proditem[0].vendorid,
                                        counter.toString())
                                    .then((response) {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                          content: Text("Added to cart")));
                                });
                              },
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        side: const BorderSide(
                                            color: Colors.green))),
                              ),
                              icon: const Icon(Icons.shopping_cart_checkout),
                              label: const Text('Add to Cart')),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
  }
}
