import 'package:flutter/material.dart';
import 'package:shopngo/routes/routes.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            DrawerHeader(child: Image.asset('assets/images/shopngo.jpeg')),
            ListTile(
              leading: const Icon(Icons.house),
              title: const Text('In house shopping'),
              onTap: (() {
                Navigator.pushNamed(context, Routes.inhouseScreen);
              }),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title: const Text('Profile'),
              onTap: (() {
                Navigator.pushNamed(context, Routes.profileScreen);
              }),
            ),
            ListTile(
              leading: const Icon(Icons.category),
              title: const Text('All Categories'),
              onTap: () {
                Navigator.pushNamed(context, Routes.categoriesScreen);
              },
            ),
            ListTile(
              leading: const Icon(Icons.help),
              title: const Text('Help'),
              onTap: (() {}),
            ),
            ListTile(
              leading: const Icon(Icons.info),
              title: const Text('About Us'),
              onTap: (() {}),
            ),
            ListTile(
              leading: const Icon(Icons.policy),
              title: const Text('Privacy Policy'),
              onTap: (() {}),
            ),
            ListTile(
              leading: const Icon(Icons.shield_moon),
              title: const Text('Terms and Conditions'),
              onTap: (() {}),
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Settings'),
              onTap: (() {}),
            ),
            ListTile(
              leading: const Icon(Icons.logout),
              title: const Text('Log Out'),
              onTap: (() {}),
            ),
          ],
        ),
      ),
    );
  }
}
