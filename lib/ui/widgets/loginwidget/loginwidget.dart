import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/routes/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginWidget extends StatefulWidget {
  const LoginWidget({super.key});

  @override
  State<LoginWidget> createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  bool isloading = false;
  void signIn(String username, String password) async {
    Map data = {'username': username, 'password': password};

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var url =
        Uri.http('apiforshopandgo.pythonanywhere.com', 'shopngo-api/login/');
    var response = await http.post(url, body: data);
    var jsonData;
    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);
      ApiServices.getUser(jsonData['token']).then((response) {
        final res = jsonDecode(response.body);
        final data = res['data'];
        sharedPreferences.setInt('id', data['id']);
        sharedPreferences.setString('username', data['username']);
        sharedPreferences.setString('email', data['email']);
      });
      setState(() {
        isloading = false;
        sharedPreferences.setString('token', jsonData['token']);
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text("Login Succesfull")));
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.homeScreen, (route) => false);
      });
    } else {
      setState(() {
        isloading = false;
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.loginScreen, (route) => false);
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
            content: Text("Please Enter correct login details")));
      });
    }
  }

  bool obscuretext = true;
  final TextEditingController usernamecontroller = TextEditingController();
  final TextEditingController passwordcontroller = TextEditingController();
  final formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return isloading
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : Center(
            child: SingleChildScrollView(
              child: Form(
                key: formkey,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/images/shopngo.jpeg',
                        width: 150,
                        height: 150,
                      ),
                      const Text(
                        'Welcome Back',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 35),
                      ),
                      const Text(
                        'Sign in with your email and password',
                        style: TextStyle(color: Colors.grey),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      TextFormField(
                        controller: usernamecontroller,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please Enter your Username';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            label: const Text('Enter Username'),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        controller: passwordcontroller,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please Enter A Password';
                          }
                          return null;
                        },
                        obscureText: obscuretext,
                        decoration: InputDecoration(
                            suffixIcon: IconButton(
                              icon: obscuretext
                                  ? const Icon(Icons.remove_red_eye)
                                  : const Icon(Icons.visibility_off),
                              onPressed: () {
                                setState(() {
                                  obscuretext = !obscuretext;
                                });
                              },
                            ),
                            label: const Text(
                              'Enter password',
                            ),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8))),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          child: const Text('Forgot Password ?'),
                          onPressed: () {},
                        ),
                      ),
                      SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: ElevatedButton(
                            onPressed: () {
                              if (formkey.currentState!.validate()) {
                                setState(() {
                                  isloading = true;
                                });
                                signIn(usernamecontroller.text,
                                    passwordcontroller.text);
                              }

                              // Navigator.pushNamedAndRemoveUntil(
                              //     context, Routes.homeScreen, (route) => false);
                            },
                            style: ButtonStyle(
                                padding: MaterialStateProperty.all<
                                        EdgeInsetsGeometry>(
                                    const EdgeInsets.all(15)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0),
                                        side: const BorderSide(
                                            color: Colors.green)))),
                            child: const Text('Continue'),
                          )),
                      const SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Don't have an account ?"),
                          TextButton(
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, Routes.signupScreen);
                              },
                              child: const Text('Sign Up'))
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}
