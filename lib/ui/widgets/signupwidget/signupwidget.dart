import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shopngo/routes/routes.dart';
import 'package:http/http.dart' as http;

class SignupWidget extends StatefulWidget {
  const SignupWidget({super.key});

  @override
  State<SignupWidget> createState() => _SignupWidgetState();
}

class _SignupWidgetState extends State<SignupWidget> {
  bool isloading = false;

  void register(String email, String username, String password) async {
    Map data = {'email': email, 'username': username, 'password': password};

    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var url =
        Uri.http('apiforshopandgo.pythonanywhere.com', 'shopngo-api/register/');
    var response = await http.post(url, body: data);
    var jsonData;
    if (response.statusCode == 200) {
      jsonData = jsonDecode(response.body);
      setState(() {
        isloading = false;
        sharedPreferences.setString('token', jsonData['token']);
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.loginScreen, (route) => false);
      });
    } else {
      setState(() {
        isloading = false;
        Navigator.pushNamedAndRemoveUntil(
            context, Routes.signupScreen, (route) => false);
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text("Please Enter correct details")));
      });
    }
  }

  final formkey = GlobalKey<FormState>();
  final TextEditingController usernamecontroller = TextEditingController();
  final TextEditingController passwordcontroller = TextEditingController();
  final TextEditingController emailcontroller = TextEditingController();
  bool obscuretext = true;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Form(
          key: formkey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/shopngo.jpeg',
                  width: 150,
                  height: 150,
                ),
                const Text(
                  'Register Account',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 35),
                ),
                const Text(
                  'Complete your registration details to continue',
                  style: TextStyle(color: Colors.grey),
                ),
                const SizedBox(
                  height: 40,
                ),
                TextFormField(
                  controller: usernamecontroller,
                  validator: ((value) {
                    if (value!.isEmpty) {
                      return 'Please enter a username';
                    }
                    return null;
                  }),
                  decoration: InputDecoration(
                      label: const Text('Enter Username'),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: emailcontroller,
                  validator: ((value) {
                    if (value!.isEmpty) {
                      return 'Please enter an email adress';
                    } else if (!value.contains('@') && !value.contains('.')) {
                      return 'Please enter a valid email';
                    }
                    return null;
                  }),
                  decoration: InputDecoration(
                      label: const Text('Enter Email'),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  controller: passwordcontroller,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the password';
                    } else if (value.length < 8) {
                      return 'Password length must be greater than 8';
                    }
                    return null;
                  },
                  obscureText: obscuretext,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: obscuretext
                            ? const Icon(Icons.remove_red_eye)
                            : const Icon(Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            obscuretext = !obscuretext;
                          });
                        },
                      ),
                      label: const Text(
                        'Enter password',
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  validator: ((value) {
                    if (value != passwordcontroller.text) {
                      return 'Password does not match';
                    } else if (value!.isEmpty) {
                      return 'Please enter a password';
                    }
                    return null;
                  }),
                  obscureText: obscuretext,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: obscuretext
                            ? const Icon(Icons.remove_red_eye)
                            : const Icon(Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            obscuretext = !obscuretext;
                          });
                        },
                      ),
                      label: const Text(
                        'Confirm Password',
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8))),
                ),
                const SizedBox(
                  height: 30,
                ),
                SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      onPressed: () {
                        if (formkey.currentState!.validate()) {
                          setState(() {
                            isloading = true;
                          });
                          register(emailcontroller.text,
                              usernamecontroller.text, passwordcontroller.text);
                        }
                      },
                      style: ButtonStyle(
                          padding:
                              MaterialStateProperty.all<EdgeInsetsGeometry>(
                                  const EdgeInsets.all(15)),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: const BorderSide(
                                          color: Colors.green)))),
                      child: const Text('Continue'),
                    )),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'By continuing you confirm that you agree with our Terms and Conditions',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Text('Already have an account ? '),
                    TextButton(
                        onPressed: () {
                          Navigator.pushNamedAndRemoveUntil(
                              context, Routes.loginScreen, (route) => false);
                        },
                        child: const Text('Login'))
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
