import 'package:flutter/widgets.dart';
import 'package:shopngo/ui/screens/bestsellersscreen.dart';
import 'package:shopngo/ui/screens/cartsscreen.dart';
import 'package:shopngo/ui/screens/categoriesscreen.dart';
import 'package:shopngo/ui/screens/categoryitemscreen.dart';
import 'package:shopngo/ui/screens/favouritescreen.dart';
import 'package:shopngo/ui/screens/homescreen.dart';
import 'package:shopngo/ui/screens/inhousescreen.dart';
import 'package:shopngo/ui/screens/itemscanscreen.dart';
import 'package:shopngo/ui/screens/loginscreen.dart';
import 'package:shopngo/ui/screens/productitemscreen.dart';
import 'package:shopngo/ui/screens/profileadressscreen.dart';
import 'package:shopngo/ui/screens/profilescreen.dart';
import 'package:shopngo/ui/screens/signupscreen.dart';
import 'package:shopngo/ui/screens/storecartsscreen.dart';
import 'package:shopngo/ui/screens/storeitemscreen.dart';

class Routes {
  static String loginScreen = 'LoginScreen';
  static String signupScreen = 'SignupScreen';
  static String homeScreen = 'HomeScreen';
  static String categoryItemScreen = 'CategoryItemScreen';
  static String productItemScreen = 'ProductItemScreen';
  static String categoriesScreen = 'CategoriesScreen';
  static String bestsellerScreen = 'BestSellersScreen';
  static String storeitemScreen = 'StoreItemScreen';
  static String favouritesScreen = 'FavouritesScreen';
  static String cartScreen = 'CartScreen';
  static String storecartScreen = 'StoreCartScreen';
  static String inhouseScreen = 'inHouseScreen';
  static String barcodeScreen = 'BarCodeScreen';
  static String profileScreen = 'ProfileScreen';
  static String profileaddresScreen = 'ProductAdressScreen';
}

Map<String, WidgetBuilder> routes = {
  Routes.loginScreen: (context) => const LoginScreen(),
  Routes.signupScreen: (context) => const SignupScreen(),
  Routes.homeScreen: (context) => const Home(),
  Routes.categoryItemScreen: (context) => const CategoryItemScreen(),
  Routes.productItemScreen: (context) => const ProductItemScreen(),
  Routes.categoriesScreen: (context) => const CategoriesScreen(),
  Routes.bestsellerScreen: (context) => const BestsellerScreen(),
  Routes.storeitemScreen: (context) => const StoreItemScreen(),
  Routes.favouritesScreen: (context) => const WishlistScreen(),
  Routes.cartScreen: (context) => const CartItemsScreen(),
  Routes.storecartScreen: (context) => const StoreCartScreen(),
  Routes.inhouseScreen: (context) => const InhouseScreen(),
  Routes.barcodeScreen: (context) => const ItemScan(),
  Routes.profileScreen: (context) => const ProfileScreen(),
  Routes.profileaddresScreen: ((context) => const ProfileAdress())
};
