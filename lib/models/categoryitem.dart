class CategoryItemModel {
  final String itemcode;
  final String itemname;
  final double offerprice;
  final double price;
  final String images;
  final String mfg;
  final int itemcategory;
  final String itembrands;
  final String description;
  final String tags;
  final String vendorid;
  final String vendorname;

  CategoryItemModel(
      this.itemcode,
      this.itemname,
      this.offerprice,
      this.price,
      this.images,
      this.mfg,
      this.itemcategory,
      this.itembrands,
      this.description,
      this.tags,
      this.vendorid,
      this.vendorname);

  CategoryItemModel.fromJson(Map json)
      : itemname = json['item_name'],
        itemcode = json['item_code'],
        offerprice = json['offer_price'],
        price = json['price'],
        images = json['item_images'],
        mfg = json['MFG'],
        itemcategory = json['item_category'],
        itembrands = json['item_brands'],
        description = json['description'],
        tags = json['tags'],
        vendorid = json['vendor_id'],
        vendorname = json['vendor_name'];
}
