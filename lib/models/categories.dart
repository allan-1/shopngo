class Categories {
  final int id;
  final String image;
  final String name;

  Categories(this.id, this.image, this.name);

  Categories.fromJson(Map json)
      : id = json['id'],
        image = json['images'],
        name = json['name'];
}
