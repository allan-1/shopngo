class StoreItemModel {
  final String vendorid;
  final String vendorname;
  final String authperson;
  final String gstno;
  final String businesstype;
  final String emailid;
  final String contactno;
  final String vendorproduct;
  final String vendorimage;
  final String adress;
  final String description;
  final String homedelivery;
  final String distance;
  final double latitude;
  final double longitude;
  final int minorder;
  final String opentime;
  final String closetime;
  final int noofproduct;
  final int storerating;
  final int noofProduct;

  StoreItemModel.fromJson(Map json)
      : vendorid = json['vendor_id'],
        vendorname = json['vendor_name'],
        authperson = json['auth_person'],
        gstno = json['GST_NO'],
        businesstype = json['business_type'],
        emailid = json['email_id'],
        contactno = json['contact_no'],
        vendorproduct = json['vendor_product'],
        vendorimage = json['vendor_profile'],
        adress = json['address'],
        description = json['description'],
        homedelivery = json['is_home_delivery'],
        distance = json['distance'],
        latitude = json['store_latitude'],
        longitude = json['store_longitude'],
        minorder = json['minimum_order'],
        opentime = json['open_time'],
        closetime = json['close_time'],
        noofproduct = json['no_of_products'],
        storerating = json['store_rating'],
        noofProduct = json['No_Of_Product'];
}
