class StoreCartItemModel {
  final String itemcode;
  final String itemname;
  final String itemimage;
  final double offerprice;
  final String description;
  final int id;
  final int quantity;
  final int consumerid;
  final String storeid;
  final String vendorid;
  final String vendorname;
  final int storerating;
  final String distance;
  final String homedelivery;

  StoreCartItemModel(
      this.itemcode,
      this.itemname,
      this.itemimage,
      this.offerprice,
      this.description,
      this.id,
      this.quantity,
      this.consumerid,
      this.storeid,
      this.vendorid,
      this.vendorname,
      this.storerating,
      this.distance,
      this.homedelivery);

  StoreCartItemModel.fromJson(Map json)
      : consumerid = json['consumer_id'],
        itemname = json['item_name'],
        itemcode = json['item_code'],
        itemimage = json['item_images'],
        offerprice = json['offer_price'],
        description = json['description'],
        id = json['id'],
        quantity = json['quantity'],
        storeid = json['store_id'],
        vendorid = json['vendor_id'],
        vendorname = json['vendor_name'],
        storerating = json['store_rating'],
        distance = json['distance'],
        homedelivery = json['is_home_delivery'];
}
