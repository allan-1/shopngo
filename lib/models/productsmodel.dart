class ProductsModel {
  final String itemcode;
  final String itemname;
  final double price;
  final String image;
  final String description;
  final String vendorid;
  final String vendorname;

  ProductsModel(
      {required this.description,
      required this.image,
      required this.itemcode,
      required this.itemname,
      required this.price,
      required this.vendorid,
      required this.vendorname});

  ProductsModel.fromJson(Map json)
      : description = json['description'],
        itemcode = json['item_code'],
        itemname = json['item_name'],
        price = json['price'],
        image = json['item_images'],
        vendorid = json['vendor_id'],
        vendorname = json['vendor_name'];
}
