// ignore_for_file: non_constant_identifier_names

class FavouritesModel {
  final int id;
  final int consumer_id;
  final String item_code;
  final String store_id;
  final String item_name;
  final String item_images;
  final double offer_price;
  final String description;
  final String vendor_id;
  final String vendor_name;

  FavouritesModel(
      this.id,
      this.consumer_id,
      this.item_code,
      this.store_id,
      this.item_name,
      this.item_images,
      this.offer_price,
      this.description,
      this.vendor_id,
      this.vendor_name);

  FavouritesModel.fromJson(Map json)
      : consumer_id = json['consumer_id'],
        id = json['id'],
        item_code = json['item_code'],
        store_id = json['store_id'],
        item_name = json['item_name'],
        item_images = json['item_images'],
        offer_price = json['offer_price'],
        description = json['description'],
        vendor_id = json['vendor_id'],
        vendor_name = json['vendor_name'];
}
