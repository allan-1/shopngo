class StoreCartModel {
  final String vendorid;
  final String vendorname;
  final String vendorimage;
  final int itemcount;

  StoreCartModel(
      this.vendorid, this.vendorname, this.vendorimage, this.itemcount);

  StoreCartModel.fromJson(Map json)
      : itemcount = json['item_count'],
        vendorid = json['vendor_id'],
        vendorname = json['vendor_name'],
        vendorimage = json['vendor_profile'];
}
