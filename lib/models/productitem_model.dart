class ProductItemModel {
  final String itemcode;
  final String itemname;
  final double price;
  final double offerprice;
  final String itemsku;
  final String color;
  final String size;
  final String weight;
  final String image;
  final String tags;
  final String mfgdate;
  final String expirydate;
  final int rating;
  final String barcodesequence;
  final String description;
  final int itemcategory;
  final int itemsubcategory;
  final String itembrands;
  final String vendorid;
  final String vendorname;

  ProductItemModel(
      this.itemcode,
      this.itemname,
      this.price,
      this.offerprice,
      this.itemsku,
      this.color,
      this.size,
      this.weight,
      this.image,
      this.tags,
      this.mfgdate,
      this.expirydate,
      this.rating,
      this.barcodesequence,
      this.description,
      this.itemcategory,
      this.itemsubcategory,
      this.itembrands,
      this.vendorid,
      this.vendorname);

  ProductItemModel.fromJson(Map json)
      : itemcode = json['item_code'],
        itemname = json['item_name'],
        price = json['price'],
        offerprice = json['offer_price'],
        itemsku = json['item_sku'],
        color = json['color'],
        size = json['size'],
        weight = json['weight'],
        image = json['item_images'],
        tags = json['tags'],
        mfgdate = json['MFG'],
        expirydate = json['expiary_date'],
        rating = json['rating'],
        barcodesequence = json['barcode_sequence'],
        description = json['description'],
        itemcategory = json['item_category'],
        itemsubcategory = json['item_sub_category'],
        itembrands = json['item_brands'],
        vendorid = json['vendor_id'],
        vendorname = json['vendor_name'];
}
