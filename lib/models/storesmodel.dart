class StoresModel {
  final String vendorid;
  final String vendorname;
  final String authperson;
  final String vendorimage;
  final String homedelivery;
  final String distance;
  final int minimumorder;
  final String opentime;
  final String closetime;
  final int noofproducts;

  StoresModel(
      this.vendorid,
      this.vendorname,
      this.authperson,
      this.vendorimage,
      this.homedelivery,
      this.distance,
      this.minimumorder,
      this.opentime,
      this.closetime,
      this.noofproducts);

  StoresModel.fromJson(Map json)
      : vendorid = json['vendor_id'],
        vendorname = json['vendor_name'],
        authperson = json['auth_person'],
        vendorimage = json['vendor_profile'],
        homedelivery = json['is_home_delivery'],
        distance = json['distance'],
        minimumorder = json['minimum_order'],
        opentime = json['open_time'],
        closetime = json['close_time'],
        noofproducts = json['No_Of_Product'];
}
