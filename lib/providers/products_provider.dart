import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/productitem_model.dart';
import 'package:shopngo/models/productsmodel.dart';

class ProductsProvider with ChangeNotifier {
  bool loading = false;
  var bestsellerlist = <ProductsModel>[];
  var bestsellerslised = <ProductsModel>[];
  var productitemlist = <ProductItemModel>[];
  var recommededlist = <ProductsModel>[];
  var recommededslised = <ProductsModel>[];
  int counter = 1;

  getBestSellers() async {
    loading = true;
    await ApiServices.getBestSeller().then((response) {
      final res = jsonDecode(response.body);
      Iterable list = res['data'];
      bestsellerlist =
          list.map((model) => ProductsModel.fromJson(model)).toList();
    });
    bestsellerslised = bestsellerlist.sublist(0, 4);
    loading = false;
    notifyListeners();
  }

  getProductItem(productid) async {
    loading = true;
    await ApiServices.getProductItem(productid).then((response) {
      final res = jsonDecode(response.body);
      Iterable list = res['data'];
      productitemlist =
          list.map((model) => ProductItemModel.fromJson(model)).toList();
    });
    loading = false;
    notifyListeners();
  }

  getRecommendedProducts() async {
    loading = true;
    await ApiServices.getRecommendedProduct().then((response) {
      final res = jsonDecode(response.body);
      Iterable list = res['data'];
      recommededlist =
          list.map((model) => ProductsModel.fromJson(model)).toList();
    });
    recommededslised = recommededlist.sublist(0, 3);
    loading = false;
    notifyListeners();
  }

  incrementCounter() {
    counter += 1;
    notifyListeners();
  }

  decrementCounter() {
    if (counter > 1) {
      counter -= 1;
    } else {
      counter = 1;
    }
    notifyListeners();
  }
}
