import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shopngo/apis/api_services.dart';
import 'package:shopngo/models/categories.dart';
import 'package:shopngo/models/categoryitem.dart';

class CategoryProvider extends ChangeNotifier {
  bool loading = false;
  var slicedlist = <Categories>[];
  var categories = <Categories>[];
  var categoryitem = <CategoryItemModel>[];
  getCategories() async {
    loading = true;
    await ApiServices.getCategoryList().then((response) {
      final res = jsonDecode(response.body);
      Iterable list = res['data'];
      categories = list.map((model) => Categories.fromJson(model)).toList();
    });
    slicedlist = categories.sublist(0, 4);
    loading = false;
    notifyListeners();
  }

  getCategoryItem(categoryid) async {
    loading = true;
    await ApiServices.getCategoryItem(categoryid).then((respone) {
      final res = jsonDecode(respone.body);
      Iterable list = res['data'];
      categoryitem =
          list.map((model) => CategoryItemModel.fromJson(model)).toList();
      loading = false;

      notifyListeners();
    });
  }
}
