import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class ApiServices {
  static Future getUser(String tokenid) {
    const String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/user-profile/consumer';
    return http.get(Uri.parse(url), headers: {
      HttpHeaders.authorizationHeader: 'Token $tokenid',
    });
  }

  static Future getStoreList() {
    const String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/store-list/';
    return http.get(Uri.parse(url));
  }

  static Future getVendor(String vendorid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/store-list/$vendorid';
    return http.get(Uri.parse(url));
  }

  static Future getVendorProducts(String vendormasters) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/vendor-product/$vendormasters';
    return http.get(Uri.parse(url));
  }

  static Future getProductList() {
    const String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/all-products/';
    return http.get(Uri.parse(url));
  }

  static Future getProductItem(String productid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/all-products/$productid';
    return http.get(Uri.parse(url));
  }

  static Future getCategoryList() {
    const String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/category/';
    return http.get(Uri.parse(url));
  }

  static Future getCategorydetails(String categoryid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/category/$categoryid';
    return http.get(Uri.parse(url));
  }

  static Future getCategoryItem(String categoryid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/category-based-product/$categoryid';
    return http.get(Uri.parse(url));
  }

  static Future getRecommendedProduct() {
    const String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/recommended';
    return http.get(Uri.parse(url));
  }

  static Future getBestSeller() {
    const String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/best-seller-products/';
    return http.get(Uri.parse(url));
  }

  static Future setFavourite(
      String consumerid, String itemcode, String storeid) {
    var url = Uri.parse(
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/favourite-products/');
    return http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          'consumer_id': consumerid,
          'item_code': itemcode,
          'store_id': storeid
        }));
  }

  static Future getFavourite(String consumerid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/favourite-products/$consumerid';
    return http.get(Uri.parse(url));
  }

  static Future addToCart(
      String consumerid, String itemcode, String storeid, String quantity) {
    var url = Uri.parse(
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/add-to-cart/');
    return http.post(url,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8'
        },
        body: jsonEncode({
          'consumer_id': consumerid,
          'item_code': itemcode,
          'store_id': storeid,
          'quantity': quantity,
        }));
  }

  static Future getStoreCarts(String consumerid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/cart-store-list/$consumerid';
    return http.get(Uri.parse(url));
  }

  static Future getStoreCartsItems(String consumerid, String vendorid) {
    final String url =
        'http://apiforshopandgo.pythonanywhere.com/shopngo-api/get-to-cart/$consumerid/$vendorid';
    return http.get(Uri.parse(url));
  }
}
